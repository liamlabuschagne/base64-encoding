#include <stdio.h>
int main()
{
    char byte = 'a';
    int i;

    unsigned int output = 0;

    for(i = 0; i < 6; ++i){
        printf("%d ", (byte >> i) & 0x01);
        output = output | ((byte >> i) & 0x01);
        output = output << 1;
    }

    printf("\n");

    for(i = 0; i < 63; i++){
        printf("%d", (output >> i) & 0x01);
    }

    printf("Size: %i Value: %u",sizeof(output),output);

    return 0;
}
